import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count : 0
    }
  }

  plus() {
    this.setState({
      count : this.state.count + 1
    })
  }

  minus() {
    this.setState({
      count : this.state.count - 1
    })
  }

  bai() {
    this.setState({
      count : this.state.count * 2
    })
  }

  hanbun() {
    this.setState({
    count : this.state.count  / 2
    })
  }

  render() {
    return (
      <span>
        <div>
          かうんと : {this.state.count}
        </div>

        <div>
          <button onClick={this.plus.bind(this)}>たす</button>
          <button onClick={this.minus.bind(this)}>ひく</button>
          <button onClick={this.bai.bind(this)}>ばい</button>
          <button onClick={this.hanbun.bind(this)}>はんぶん</button>
        </div>
      </span>
    )
  }
}

export default App;
